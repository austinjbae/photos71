package photos.app;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * A tag object to describe photo attributes
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class Tag implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public String category;
	public String name;
	
	/**
	 * Constructs a tag with the specified parameters
	 * @param cat tag category
	 * @param nam tag name
	 */
	public Tag(String cat, String nam) {
		category = cat;
		name = nam;
	}

}
