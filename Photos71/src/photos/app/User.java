package photos.app;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * User object to hold user photo albums
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class User implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public String userName;
	public ArrayList<Album> albums;
	ArrayList<Photo> photos;
	public ArrayList<String> presets;
	
	/**
	 * Constructs a user object with the specified name
	 * @param name user name
	 */
	public User(String name) {
		userName = name;
		albums = new ArrayList<Album>();
		photos = new ArrayList<Photo>();
		
		presets = new ArrayList<String>();
		presets.add("location");
		presets.add("person");
		presets.add("color");
	}
	
	/**
	 * Returns the name of the user
	 * @return String user name
	 */
	public String getName() {
		return this.userName;
	}
	
	
}
