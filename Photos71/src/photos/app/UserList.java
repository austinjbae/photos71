package photos.app;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Data structure to store all users and user data
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class UserList implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	ArrayList<User> userList;
	ArrayList<String> userListString;
	
	/**
	 * Constructs an empty UserList
	 */
	public UserList() {
		this.userList = new ArrayList<User>();
		this.userListString = new ArrayList<String>();
	}
	
	/**
	 * Returns an ArrayList of users stored in UserList
	 * @return ArrayList of users
	 */
	public ArrayList<User> getUserList(){
		return userList;
	}
	
	/**
	 * Returns an ArrayList of strings representing names in UserList
	 * @return ArrayList of username strings
	 */
	public ArrayList<String> getUserListString() {
		return userListString;
	}
	
	/**
	 * Adds inputed user to UserList
	 * @param newUser user object
	 */
	public void addUser(User newUser) {
		
		if(userList.size() == 0) {
			userList.add(newUser);
			userListString.add(newUser.getName());
		}
		else {
			for(int i = 0; i < userList.size(); i++) {
				if(userList.get(i).userName == newUser.userName) {
					throw new IllegalArgumentException();
				}
			}
			userList.add(newUser);
			userListString.add(newUser.getName());
		}
		
	}
	
	/**
	 * Deletes specified user from UserList
	 * @param currentUser user object
	 */
	public void deleteUser(User currentUser) {
		if(userList.size() == 0) {
			return;
		}
		for(int i = 0; i < userList.size(); i++) {
			if(userList.get(i).userName == currentUser.userName) {
				userList.remove(i);
				userListString.remove(currentUser.getName());
			}
		}
	}

}
