package photos.app;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Album object for storing photos
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class Album implements Serializable{

	private static final long serialVersionUID = 1L;
	public String albumName;
	public ArrayList<Photo> photos;
	
	
	/**
	 * Constructs Album object
	 * @param name name of album
	 */
	public Album(String name) {
		albumName = name;
		photos = new ArrayList<Photo>();
	}
	
	/**
	 * 
	 * Constructs Album Object
	 * @param name
	 * @param userPhotos ArrayList of photos
	 */
	public Album(String name, ArrayList<Photo> userPhotos) {
		albumName = name;
		photos = userPhotos;
	}
	
	/**
	 * 
	 * Renames Album object
	 * @param newName Name of album
	 */
	public void renameAlbum(String newName) {
		albumName = newName;
	}
	
	/**
	 * Returns album name
	 * @return Album name
	 */
	public String getAlbumName() {
		return albumName;
	}
	
	/**
	 * returns size of album
	 * @return integer size of album
	 */
	public Integer getAlbumSize() {
		return photos.size();
	}
	
	/**
	 * returns earliest photo date in album
	 * @return String earliest date
	 */
	public String getEarliestDate() {
		if(photos.size() == 0) {
			return null;
		}
		Calendar earliest = photos.get(0).calendar;
		for(int i = 0; i < photos.size(); i++) {
			if(photos.get(i).calendar.compareTo(earliest) < 1) {
				earliest = photos.get(i).calendar;
			}
		}
		SimpleDateFormat format1 = new SimpleDateFormat("MM-dd-yyyy");
		return format1.format(earliest.getTime());
	}
	
	/**
	 * returns the latest photo date in album
	 * @return String latest date
	 */
	public String getLatestDate() {
		if(photos.size() == 0) {
			return null;
			
		}
		Calendar latest = photos.get(0).calendar;
		for(int i = 0; i < photos.size(); i++) {
			if(photos.get(i).calendar.compareTo(latest) > 1) {
				latest = photos.get(i).calendar;
			}
		}
		SimpleDateFormat format1 = new SimpleDateFormat("MM-dd-yyyy");
		return format1.format(latest.getTime());
		
	}
	
	/**
	 * Adds photo object to album
	 * @param newPhoto photo to add to album
	 */
	public void addPhoto(Photo newPhoto) {
		//check for duplicate photo, then add
		photos.add(newPhoto);
	}
	
	/**
	 * returns true if photo already exists in album and false otherwise
	 * @param newPhoto photo to compare against album
	 * @return true if photo already exists
	 */
	public Boolean duplicatePhoto(Photo newPhoto) {
		if(photos.contains(newPhoto)) {
			return true;
		}
		else {
			for(int i = 0; i < photos.size(); i++) {
				if(newPhoto.photo.equals(photos.get(i).photo)) {
					return true;
				}
			}
			return false;
		}
	}

	

}
