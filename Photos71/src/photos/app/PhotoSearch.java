package photos.app;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Contains methods for searching through user photos for specific attributes
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class PhotoSearch {
	
	/**
	 * Returns an ArrayList of photos whose dates fall in the specified range
	 * @param currentUser current user object
	 * @param beginDate String of date
	 * @param endDate String of date
	 * @return ArrayList of search results
	 * @throws ParseException
	 */
	public static ArrayList<Photo> photoSearch(User currentUser, String beginDate, String endDate) throws ParseException{
		ArrayList<Photo> searchResults = new ArrayList<Photo>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(sdf.parse(beginDate));
		cal2.setTime(sdf.parse(endDate));
		
		Photo currentPhoto;
		
		for(int i = 0; i < currentUser.albums.size(); i++) {
			for(int j = 0; j < currentUser.albums.get(i).photos.size(); j++) {
				currentPhoto = currentUser.albums.get(i).photos.get(j);
				if(currentPhoto.calendar.compareTo(cal1) > 0 && currentPhoto.calendar.compareTo(cal2) < 0) {
					if(!searchResults.contains(currentPhoto)) {
					searchResults.add(currentPhoto);
					}
				}
			}
		}
		
		return searchResults;
	}
	
	/**
	 * Returns an ArrayList of photo which have a tag matching the specified parameters
	 * @param currentUser current user object
	 * @param desiredTag tag object
	 * @return ArrayList of search results
	 */
	public static ArrayList<Photo> photoSearch(User currentUser, Tag desiredTag){
		ArrayList<Photo> searchResults = new ArrayList<Photo>();
		
		Photo currentPhoto;
		
		for(int i = 0; i < currentUser.albums.size(); i++) {
			for(int j = 0; j < currentUser.albums.get(i).photos.size(); j++) {
				currentPhoto = currentUser.albums.get(i).photos.get(j);
				if(currentPhoto.searchTags(desiredTag.category, desiredTag.name)) {
					if(!searchResults.contains(currentPhoto)) {
						searchResults.add(currentPhoto);
					}
				}
			}
		}
		return searchResults;
	}
	
	/**
	 * Returns an ArrayList of photos which have a tag matching either specified tag
	 * @param currentUser current user object
	 * @param tag1 tag object
	 * @param tag2 tag object
	 * @return ArrayList of search results
	 */
	public static ArrayList<Photo> photoSearchDisjunctive(User currentUser, Tag tag1, Tag tag2){
		ArrayList<Photo> searchResults = new ArrayList<Photo>();
		
		Photo currentPhoto;
		
		for(int i = 0; i < currentUser.albums.size(); i++) {
			for(int j = 0; j < currentUser.albums.get(i).photos.size(); j++) {
				currentPhoto = currentUser.albums.get(i).photos.get(j);
				if(currentPhoto.searchTags(tag1.category, tag1.name) || currentPhoto.searchTags(tag2.category, tag2.name)) {
					if(!searchResults.contains(currentPhoto)) {
						searchResults.add(currentPhoto);
					}
				}
			}
		}
		
		return searchResults;
	}
	
	/**
	 * Returns an ArrayList of photos which have a tags matching both specified tags
	 * @param currentUser current user object
	 * @param tag1 tag object
	 * @param tag2 tag object
	 * @return ArrayList of search results
	 */
	public static ArrayList<Photo> photoSearchConjunctive(User currentUser, Tag tag1, Tag tag2){
		ArrayList<Photo> searchResults = new ArrayList<Photo>();
		
		Photo currentPhoto;
		
		for(int i = 0; i < currentUser.albums.size(); i++) {
			for(int j = 0; j < currentUser.albums.get(i).photos.size(); j++) {
				currentPhoto = currentUser.albums.get(i).photos.get(j);
				if(currentPhoto.searchTags(tag1.category, tag1.name) && currentPhoto.searchTags(tag2.category, tag2.name)) {
					if(!searchResults.contains(currentPhoto)) {
						searchResults.add(currentPhoto);
					}
				}
			}
		}
		
		return searchResults;
	}

}
