package photos.app;

import java.io.*;

/**
 * Contains methods for storing and reading serialized object data
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class DataStorage{
	
	static final String storeDir = "data";
	static final String storeFile = "UserList.dat";
	
	/**
	 * Serializes UserList data and writes to disk
	 * @param users UserList of all users
	 * @throws IOException
	 */
	public static void writeData(UserList users) throws IOException{
		
		ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream(storeDir + File.separator + storeFile));
		oos.writeObject(users);
		oos.close();
		
	}
	
	/**
	 * Reads serialized UserList data from disk
	 * @return UserList of all stored user data
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static UserList readData() throws IOException, ClassNotFoundException{
		
		ObjectInputStream ois = new ObjectInputStream(
				new FileInputStream(storeDir + File.separator + storeFile));
		UserList readUsers = (UserList)ois.readObject();
		ois.close();
		return readUsers;
		
	}

}
