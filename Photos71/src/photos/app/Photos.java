package photos.app;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Calendar;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import photos.view.LoginController;

/**
 * contains main and start methods
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class Photos extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/photos/view/photosLogin.fxml"));
		//loader.setController(new InterfaceController());
		AnchorPane root = (AnchorPane) loader.load();
		
		LoginController interfaceController = loader.getController();
		interfaceController.start();
		
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public static void main(String[] args){
		
		/*
		Path file = Paths.get("/Users/ianmartin/Downloads/LongerIntro.pdf");
		BasicFileAttributes attr = null;
		try {
			attr = Files.readAttributes(file, BasicFileAttributes.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(attr.lastModifiedTime());
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(attr.lastModifiedTime().toMillis());
		System.out.println(calendar.get(Calendar.YEAR));
		System.out.println(calendar.get(Calendar.DATE));
		System.out.println(calendar.get(Calendar.MONTH));
		System.out.println(calendar.get(Calendar.SECOND));
		*/
		launch(args);
		
	}


}
