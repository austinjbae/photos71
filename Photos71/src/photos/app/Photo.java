package photos.app;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Photo object for storying and displaying photo data
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class Photo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public File photo;
	public String caption;
	public ArrayList<Tag> tags;
	Calendar calendar;
	
	/**
	 * Constructs photo object
	 * @param userPhoto new photo
	 */
	public Photo(File userPhoto) {
		this.photo = userPhoto;
		//this.caption = userPhoto.getName();
		this.tags = new ArrayList<Tag>();
		this.calendar = Calendar.getInstance();
		Date date = new Date(userPhoto.lastModified());
		calendar.setTime(date);
		calendar.set(Calendar.MILLISECOND, 0);
	}
	
	/**
	 * Constructs photo object from string
	 * @param userPhoto String of new photo's file path
	 */
	public Photo(String userPhoto) {
		this.photo = new File(userPhoto);
		//this.caption = userPhoto.getName();
		this.tags = new ArrayList<Tag>();
		this.calendar = Calendar.getInstance();
		Date date = new Date(photo.lastModified());
		calendar.setTime(date);
		calendar.set(Calendar.MILLISECOND, 0);
	}
	
	/**
	 * adds tag to tags ArrayList
	 * @param category tag category
	 * @param name tag name
	 */
	public void addTag(String category, String name) {
		tags.add(new Tag(category, name));
//		boolean duplicate = false;
//		if(tags.size() == 0) {
//			tags.add(new Tag(category, name));
//		}
//		for(int i = 0; i < tags.size(); i++) {
//			if(tags.get(i).category == category && tags.get(i).name == name) {
//				duplicate = true;
//			}
//		}
//		if(duplicate == false) {
//			tags.add(new Tag(category, name));
//		}
	}
	
	
	/**
	 * removes tag from tags ArrayList
	 * @param deleteTag tag to be deleted
	 */
	public void deleteTag(Tag deleteTag) {
		
		for(int i = 0; i < tags.size(); i++) {
			if(tags.get(i) == deleteTag) {
				tags.remove(i);
				break;
			}
		}
		
	}
	
	/**
	 * returns date photo file was last modified as a string
	 * @return String of photo's date
	 */
	public String getPhotoDate() {
		SimpleDateFormat format1 = new SimpleDateFormat("MM-dd-yyyy");
		return format1.format(calendar.getTime());
	}
	
	/**
	 * returns Photo's caption as a string
	 * @return String of photo's caption
	 */
	public String getCaption() {
		return caption;
	}
	
	/**
	 * returns true if new tag data matches existing tag in tags ArrayList
	 * @param category tag category
	 * @param name tag name
	 * @return Boolean
	 */
	public Boolean searchTags(String category, String name) {
		Boolean containsTag = false;
		for(int i = 0; i < tags.size(); i++) {
			if(tags.get(i).category.equals(category) && tags.get(i).name.equals(name)) {
				containsTag = true;
			}
		}
		return containsTag;
	}
	
	/**
	 * returns true if a tag in tags ArrayList has category "location"
	 * @return Boolean
	 */
	public Boolean containsLoctionTag() {
		for(int i = 0; i < tags.size(); i++) {
			if(tags.get(i).category.equals("location")) {
				return true;
			}
		}
		return false;
	}
	
	

}
