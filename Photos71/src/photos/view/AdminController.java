package photos.view;

import java.io.IOException;

import photos.app.DataStorage;
import photos.app.User;
import photos.app.UserList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * This class controls the photosAdmin.fxml view
 * @author ianmartin
 *
 */
public class AdminController{
	
	private Stage stage;
	private Scene scene;
	private Parent rootParent;
	private UserList userList;
	ObservableList<String> usernames = FXCollections.observableArrayList();
	
    @FXML
    private ListView<String> listviewUsers;
    @FXML
    private TextField adminTextfield;
    @FXML
    private Button addUser;
    @FXML
    private Button deleteUser;
    @FXML
    private Button adminExit;

    /**
     * Logs the user out
     * Switches the scene to the login
     * @param event
     * @throws IOException
     */
    public void switchToLogin(ActionEvent event) throws IOException {
    	
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("/photos/view/photosLogin.fxml"));
    	Parent rootParent = loader.load();

    	LoginController loginController = loader.getController();
    	loginController.loadUserList(this.userList);
    	
    	DataStorage.writeData(userList);
    	
    	stage = (Stage)((Node)event.getSource()).getScene().getWindow();
    	scene = new Scene(rootParent);
    	stage.setScene(scene);
    	stage.show();
    }
    
    /**
     * Loads the UserList for AdminController from the UserList object passed by the LoginController
     * @param userList
     */
    public void populateListView(UserList userList) {
    	this.userList = userList;
    	
    	if(userList.getUserListString() != null) {
    		usernames = FXCollections.observableArrayList(userList.getUserListString());
    		listviewUsers.setItems(usernames);
    	}
    }
    
    /**
     * Creates and Adds a new User to the UserList if the inputed name isn't already a User
     * Updates the ObservableList accordingly
     * @param event
     */
    public void addUser(ActionEvent event) {
    	String newUser = adminTextfield.getText();
    	
    	if(newUser != null && !newUser.isEmpty()) {		
    		if(!usernames.contains(newUser)) {
    			usernames.add(newUser);
    			
    			User newuser = new User(newUser);
    			this.userList.addUser(newuser);
    		} else {
    			//notify user user already exists
    		}
    	} else {
    		Alert alert = new Alert(AlertType.ERROR, "Invalid User Name");
    		alert.showAndWait();
    	}
    	
    	adminTextfield.clear();
    }
    
    /**
     * Removes the selected User from the UserList.
     * Updates the ObservableList accordingly
     * @param event
     */
    public void deleteUser(ActionEvent event) {
    	String selectedUser = listviewUsers.getSelectionModel().getSelectedItem();
    	if(selectedUser != null && !selectedUser.isEmpty()) {
    		if(!selectedUser.equals("stock")) {
	    		for(int i = 0; i < this.userList.getUserList().size(); i++) {
	    			if(selectedUser.equals(this.userList.getUserList().get(i).getName())) {
	    				User deleteUser = this.userList.getUserList().get(i);
	    				this.userList.deleteUser(deleteUser);
	    				
	    				usernames.remove(selectedUser);
	    				break;
	    			}
	    		}
    		} else {
        		Alert alert = new Alert(AlertType.ERROR, "Cannot Delete Stock User");
        		alert.showAndWait();
    		}
    		
    	} else {
    		Alert alert = new Alert(AlertType.ERROR, "Select a User To Delete");
    		alert.showAndWait();
    	}
    }

}
