package photos.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import photos.app.Album;
import photos.app.Photo;
import photos.app.User;

/**
 * This class controls the copyMoveView.fxml view
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class CopyMoveViewController {
	
	@FXML
	private ListView<String> albumListView;
	@FXML
	private Button btnCancel;
	@FXML
	private Button btnCopy;
	
	User currentUser;
	Photo currentPhoto;
	Album currentAlbum;
	Boolean isMove;
	private ObservableList<String> obsList = FXCollections.observableArrayList();
	
	/**
	 * This method initializes the TableView and class fields
	 * Populates TableView with photo data
	 * @param currentUser
	 * @param currentPhoto
	 * @param currentAlbum
	 * @param isMove
	 */
	public void setupList(User currentUser, Photo currentPhoto, Album currentAlbum, Boolean isMove) {
		
		if(isMove) {
			btnCopy.setText("Move");
		}
		
		this.currentUser = currentUser;
		this.currentPhoto = currentPhoto;
		this.currentAlbum = currentAlbum;
		this.isMove = isMove;
		for(int i = 0; i < currentUser.albums.size(); i++) {
			obsList.add(currentUser.albums.get(i).albumName);
		}
		
		albumListView.setItems(obsList);
		albumListView.getSelectionModel().select(0);
		
		
		
	}
	
	/**
	 * Copies photo to new album when copy button is pressed
	 * @param event
	 */
	public void copyPhoto(ActionEvent event) {
		Alert nullSelectionAlert = new Alert(AlertType.ERROR);
		nullSelectionAlert.setTitle("Selection Error");
		nullSelectionAlert.setHeaderText("Select an item");
		nullSelectionAlert.setContentText("An item must be selected in the list before taking action");
		
		Alert duplicatePhotoAlert = new Alert(AlertType.ERROR);
		duplicatePhotoAlert.setTitle("Duplicate Photo");
		duplicatePhotoAlert.setHeaderText("Photo already exists");
		duplicatePhotoAlert.setContentText("A photo may not be added to an album more than once");
		if(albumListView.getSelectionModel().getSelectedItem() == null) {
    		nullSelectionAlert.showAndWait();
    	}
    	else {
    	if(currentUser.albums.get(albumListView.getSelectionModel().getSelectedIndex()).duplicatePhoto(currentPhoto)){
    		duplicatePhotoAlert.showAndWait();
    	}
    	else {
		currentUser.albums.get(albumListView.getSelectionModel().getSelectedIndex()).addPhoto(currentPhoto);
		if(isMove) {
			currentAlbum.photos.remove(currentAlbum.photos.indexOf(currentPhoto));
		}
		closeStage(event);
    	}
    	}
	}
	
	/**
	 * Moves photo to new album when move button is pressed
	 * @param event
	 */
	public void movePhoto(ActionEvent event) {
		Alert nullSelectionAlert = new Alert(AlertType.ERROR);
		nullSelectionAlert.setTitle("Selection Error");
		nullSelectionAlert.setHeaderText("Select an item");
		nullSelectionAlert.setContentText("An item must be selected in the list before taking action");
		
		Alert duplicatePhotoAlert = new Alert(AlertType.ERROR);
		duplicatePhotoAlert.setTitle("Duplicate Photo");
		duplicatePhotoAlert.setHeaderText("Photo already exists");
		duplicatePhotoAlert.setContentText("A photo may not be added to an album more than once");
		if(albumListView.getSelectionModel().getSelectedItem() == null) {
    		nullSelectionAlert.showAndWait();
    	}
    	else {
    	if(currentUser.albums.get(albumListView.getSelectionModel().getSelectedIndex()).duplicatePhoto(currentPhoto)){
        	duplicatePhotoAlert.showAndWait();
        }
        else {
		currentUser.albums.get(albumListView.getSelectionModel().getSelectedIndex()).addPhoto(currentPhoto);
		currentAlbum.photos.remove(currentAlbum.photos.indexOf(currentPhoto));
		closeStage(event);
    	}
    	}
	}
	
	/**
	 * Cancels move or copy action
	 * @param event
	 */
	public void cancelMove(ActionEvent event) {
		closeStage(event);
	}
	
	/**
	 * Closes window
	 * @param event
	 */
	private void closeStage(ActionEvent event) {
		Node source = (Node)event.getSource();
		Stage stage = (Stage)source.getScene().getWindow();
		stage.close();
	}

}
