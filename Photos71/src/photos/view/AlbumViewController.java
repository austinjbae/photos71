package photos.view;

import java.io.File;
import java.io.IOException;

import photos.app.Album;
import photos.app.DataStorage;
import photos.app.Photo;
import photos.app.User;
import photos.app.UserList;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * This class controls the albumView.fxml view
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class AlbumViewController{
	
	private Stage stage;
	private Scene scene;
	private Parent rootParent;
	private User user;
	private UserList userList;
	
    @FXML
    private Button btnCreate;
    @FXML
    private Button btnDelete;
    @FXML
    private Button btnRename;
    @FXML
    private Button btnQuit;
    @FXML
    private Button btnLogOut;
    @FXML
    private TableView<Album> photoTable;
    @FXML
    private Label photoLibrary;
    @FXML
    private TextField searchTextfield;
    @FXML
    private Button btnSearch;
    @FXML
    private Button btnOpenAlbum;
    @FXML
    private TableColumn<Album, String> nameCol;
    @FXML
    private TableColumn<Album, Integer> numberCol;
    @FXML
    private TableColumn<Album, String> earliestCol;
    @FXML
    private TableColumn<Album, String> latestCol;
    
    
    private ObservableList<Album> albumsArrayList = FXCollections.observableArrayList();
    
    String newName;
    ArrayList<Album> userAlbums;
    TextInputDialog nameInputDialog = new TextInputDialog();
    TextInputDialog renameInputDialog = new TextInputDialog();
    Alert deleteConfirm = new Alert(AlertType.CONFIRMATION);
    
	/**
	 * Initializes window and table with user albums
	 * Contains actions for creating, deleting, and renaming albums
	 * @param userList
	 * @param user
	 */
	public void loadUser(UserList userList, User user) {
		this.userList = userList;
		this.user = user;
		photoLibrary.setText(this.user.getName() + "'s Photo Albums");
		
		userAlbums = user.albums;
//		userAlbums.add(new Album("Album 1"));
//		userAlbums.add(new Album("Album 2"));
//		userAlbums.add(new Album("Album 3"));			
		
		
		for(int i = 0; i < user.albums.size(); i++) {
			albumsArrayList.add(user.albums.get(i));
		}
	
			
		photoTable.setItems(albumsArrayList);		
		photoTable.getColumns().setAll(nameCol, numberCol, earliestCol, latestCol);  //addAll(nameCol);
		
		nameCol.setCellValueFactory(new PropertyValueFactory<Album, String>("albumName"));
		numberCol.setCellValueFactory(new PropertyValueFactory<Album, Integer>("albumSize"));
		earliestCol.setCellValueFactory(new PropertyValueFactory<Album, String>("earliestDate"));
		latestCol.setCellValueFactory(new PropertyValueFactory<Album, String>("latestDate"));
		
		photoTable.getSelectionModel().select(0);
		int index = photoTable.getSelectionModel().getSelectedIndex();
		
		Alert nullSelectionAlert = new Alert(AlertType.ERROR);
		nullSelectionAlert.setTitle("Selection Error");
		nullSelectionAlert.setHeaderText("Select an item");
		nullSelectionAlert.setContentText("An item must be selected in the list before taking action");
		
		nameInputDialog.setContentText("Name: ");
		nameInputDialog.setHeaderText("Enter Album Name");
		nameInputDialog.setTitle("Album Name");
		btnCreate.setOnAction(e -> {
	    	nameInputDialog.showAndWait();
	    	newName = nameInputDialog.getEditor().getText();
	    	Album newAlbum = new Album(newName);
	    	albumsArrayList.add(newAlbum);
	    	userAlbums.add(newAlbum);
	    });
		
		renameInputDialog.setContentText("Name: ");
		renameInputDialog.setHeaderText("Enter New Album Name");
		renameInputDialog.setTitle("Rename Album");
		btnRename.setOnAction(e -> {
	    	if(photoTable.getSelectionModel().getSelectedItem() == null) {
	    		nullSelectionAlert.showAndWait();
	    	}
	    	else {
	    	renameInputDialog.showAndWait();
	    	newName = renameInputDialog.getEditor().getText();
	    	photoTable.getSelectionModel().getSelectedItem().albumName = newName;
	    	photoTable.refresh();
	    	}
	    });
		
		
		deleteConfirm.setTitle("Confirm Deletion");
		deleteConfirm.setHeaderText("Delete Album?");
		deleteConfirm.setContentText("Are you sure you want to delete the selected album?");
		btnDelete.setOnAction(e -> {
			if(photoTable.getSelectionModel().getSelectedItem() == null) {
	    		nullSelectionAlert.showAndWait();
	    	}
	    	else {
			deleteConfirm.showAndWait();
			userAlbums.remove(userAlbums.indexOf(photoTable.getSelectionModel().getSelectedItem()));
			albumsArrayList.remove(albumsArrayList.indexOf(photoTable.getSelectionModel().getSelectedItem()));
	    	}
		});
		
		btnOpenAlbum.setOnAction(e -> {
			if(photoTable.getSelectionModel().getSelectedItem() == null) {
	    		nullSelectionAlert.showAndWait();
	    	}
	    	else {
			try {
				switchToPhoto(e, photoTable.getSelectionModel().getSelectedItem());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	    	}
		});
		
	}
	
	/**
	 * Changes view to search view when search button is pressed
	 * @param event
	 * @throws IOException
	 */
	public void switchToSearch(ActionEvent event) throws IOException{
		FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("/photos/view/photoSearchView.fxml"));
    	Parent rootParent = loader.load();

    	PhotoSearchViewController photoSearchViewController = loader.getController();
    	photoSearchViewController.initialize(this.userList, this.user);
    	
    	stage = (Stage)((Node)event.getSource()).getScene().getWindow();
    	scene = new Scene(rootParent);
    	stage.setScene(scene);
    	stage.show();
	}
	
	/**
	 * Returns to login window when logout button is pressed
	 * @param event
	 * @throws IOException
	 */
    public void switchToLogin(ActionEvent event) throws IOException {
    	DataStorage.writeData(userList);
    	
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("/photos/view/photosLogin.fxml"));
    	Parent rootParent = loader.load();

    	LoginController loginController = loader.getController();
    	loginController.loadUserList(this.userList);
    	
    	stage = (Stage)((Node)event.getSource()).getScene().getWindow();
    	scene = new Scene(rootParent);
    	stage.setScene(scene);
    	stage.show();
    }
    
    /**
     * Switches to photo view when open album button is pressed
     * @param event
     * @param selectedAlbum
     * @throws IOException
     */
    public void switchToPhoto(ActionEvent event, Album selectedAlbum) throws IOException{
    	if(selectedAlbum == null) {
    		Alert nullSelectionAlert = new Alert(AlertType.ERROR);
    		nullSelectionAlert.setTitle("Selection Error");
    		nullSelectionAlert.setHeaderText("Select an item");
    		nullSelectionAlert.setContentText("An item must be selected in the list before taking action");
    		nullSelectionAlert.showAndWait();
    	}
    	else {
    	FXMLLoader loader  = new FXMLLoader();
    	loader.setLocation(getClass().getResource("/photos/view/photoView.fxml"));
    	Parent rootParent = loader.load();
    	
    	PhotoViewController photoViewController = loader.getController();
    	photoViewController.loadAlbum(this.userList, this.user, selectedAlbum);
    	
    	stage = (Stage)((Node)event.getSource()).getScene().getWindow();
    	scene = new Scene(rootParent);
    	stage.setScene(scene);
    	stage.show();
    	}
    }
    
    /**
     * Closes and stops application when quit button is pressed
     * @param event
     * @throws IOException
     */
    public void quitApplication(ActionEvent event) throws IOException {
    	DataStorage.writeData(userList);
    	stage = (Stage)((Node)event.getSource()).getScene().getWindow();
    	stage.close();
    }

}
