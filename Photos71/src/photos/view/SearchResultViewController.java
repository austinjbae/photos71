package photos.view;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import photos.app.Album;
import photos.app.Photo;
import photos.app.User;
import photos.app.UserList;

/**
 * This class controls the searchResultsView.fxml view
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class SearchResultViewController {
	
	
	@FXML
	private Button btnBack;
	@FXML
	private Button btnCreate;
	@FXML
	private TableView<Photo> photoTable;
	@FXML
	private TableColumn<Photo, String> dateCol;
	@FXML
	private TableColumn<Photo, String> captionCol;
	@FXML
	private ImageView imageView;
	@FXML
	private ListView<String> tagsListView;
	
	UserList userList;
	User currentUser;
	Image image;
	ArrayList<Photo> searchResults;
	Stage stage;
	Scene scene;
	private ObservableList<Photo> photosArrayList = FXCollections.observableArrayList();
	private ObservableList<String> tagArrayList = FXCollections.observableArrayList();
	TextInputDialog nameInputDialog = new TextInputDialog();
	
	/**
	 * Initializes table with search results
	 * @param userList
	 * @param currentUser
	 * @param searchResults
	 */
	public void initialize(UserList userList, User currentUser, ArrayList<Photo> searchResults) {
		this.userList = userList;
		this.currentUser = currentUser;
		this.searchResults = searchResults;
		
		for(int i = 0; i < searchResults.size(); i++) {
			photosArrayList.add(searchResults.get(i));;
		}
		
		photoTable.setItems(photosArrayList);
		photoTable.getColumns().setAll(dateCol, captionCol);
		
		dateCol.setCellValueFactory(new PropertyValueFactory<Photo, String>("photoDate"));
		captionCol.setCellValueFactory(new PropertyValueFactory<Photo, String>("caption"));
		
		photoTable.getSelectionModel().select(0);
		
		tagsDisplay();	
		
		showInfo();
		photoTable.getSelectionModel().selectedIndexProperty().addListener(e -> {
			showInfo();
			tagsDisplay();
		});
	}
	
	/**
	 * Changes window to album view
	 * @param event
	 */
	public void backToAlbums(ActionEvent event) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/photos/view/albumView.fxml"));
		Parent rootParent = null;
		try {
			rootParent = loader.load();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		AlbumViewController albumViewController = loader.getController();
		albumViewController.loadUser(userList, currentUser);
		
		Node source = (Node)event.getSource();
		stage = (Stage)source.getScene().getWindow();
		scene = new Scene(rootParent);
		stage.setScene(scene);
		stage.show();
	}
	
	/**
	 * Turns the search results into new album with specified name
	 * @param event
	 */
	public void makeAlbum(ActionEvent event) {
		nameInputDialog.setContentText("Name: ");
		nameInputDialog.setHeaderText("Enter Album Name");
		nameInputDialog.setTitle("Album Name");
		nameInputDialog.showAndWait();
		String name = nameInputDialog.getEditor().getText();
		currentUser.albums.add(new Album(name, searchResults));
		backToAlbums(event);
	}
	
	/**
	 * Initializes list of tags for selected photo
	 */
	private void tagsDisplay() {
		if(photoTable.getSelectionModel().getSelectedItem() != null) {
		tagArrayList = FXCollections.observableArrayList();
		for(int i = 0; i < photoTable.getSelectionModel().getSelectedItem().tags.size(); i++) {
			tagArrayList.add(photoTable.getSelectionModel().getSelectedItem().tags.get(i).category + ": " + photoTable.getSelectionModel().getSelectedItem().tags.get(i).name);
		}
		
		tagsListView.setItems(tagArrayList);
		}
	}
	
	/**
	 * Displays selected photo
	 */
	private void showInfo() {
		if(photosArrayList.size() > 0) {
			InputStream stream;
			try {
				stream = new FileInputStream(photoTable.getSelectionModel().getSelectedItem().photo);
				image = new Image(stream);
				imageView.setImage(image);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			}
			}
		else {
			imageView.setImage(null);
		}
	}
	

}
