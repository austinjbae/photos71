package photos.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import photos.app.Photo;

/**
 * This class controls the deleteTagView.fxml view
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class DeleteTagViewController {
	
	@FXML
	private ListView<String> tagListView;
	@FXML
	private Button btnCancel;
	@FXML
	private Button btnDelete;
	
	Photo currentPhoto;
	private ObservableList<String> obsList = FXCollections.observableArrayList();
	
	/**
	 * Initializes list of tags and class fields
	 * @param currentPhoto
	 */
	public void setupList(Photo currentPhoto) {
		this.currentPhoto = currentPhoto;
		if(currentPhoto.tags.size()>0) {
		for(int i = 0; i < currentPhoto.tags.size(); i++) {
			obsList.add(currentPhoto.tags.get(i).category + ": " + currentPhoto.tags.get(i).name);
		}
		}
		
		tagListView.setItems(obsList);
		tagListView.getSelectionModel().select(0);
	}
	
	/**
	 * Deletes tag from list when Delete button is pressed
	 * @param event
	 */
	public void deleteTag(ActionEvent event) {
		Alert nullSelectionAlert = new Alert(AlertType.ERROR);
		nullSelectionAlert.setTitle("Selection Error");
		nullSelectionAlert.setHeaderText("Select an item");
		nullSelectionAlert.setContentText("An item must be selected in the list before taking action");
		if(tagListView.getSelectionModel().getSelectedItem() == null) {
    		nullSelectionAlert.showAndWait();
    	}
    	else {
		currentPhoto.tags.remove((tagListView.getSelectionModel().getSelectedIndex()));
		obsList.remove((tagListView.getSelectionModel().getSelectedIndex()));
		closeStage(event);
    	}
	}
	
	/**
	 * Closes window
	 * @param event
	 */
	public void cancelTag(ActionEvent event) {
		closeStage(event);
	}
	
	/**
	 * Closes window after action is taken
	 * @param event
	 */
	private void closeStage(ActionEvent event) {
		Node source = (Node)event.getSource();
		Stage stage = (Stage)source.getScene().getWindow();
		stage.close();
	}

}
