package photos.view;

import java.io.IOException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import photos.app.Photo;
import photos.app.User;

/**
 * This class controls the tagEditorView.fxml view
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class TagEditorViewController {
	
	@FXML
	private Button btnCreateTag;
	@FXML
	private Button btnCustomCategory;
	@FXML
	private TextField valueField;
	@FXML
	private ListView<String> categoryListView;
	
	Photo currentPhoto;
	User currentUser;
	private ObservableList<String> obsList = FXCollections.observableArrayList();
	private Stage stage;
	private Scene scene;
	
	/**
	 * Initializes class fields and list of tags
	 * @param currentPhoto
	 * @param currentUser
	 */
	public void initialize(Photo currentPhoto, User currentUser) {
		
		this.currentPhoto = currentPhoto;
		this.currentUser = currentUser;
		
		for(int i = 0; i < currentUser.presets.size(); i++) {
			obsList.add(currentUser.presets.get(i));
		}
		
		categoryListView.setItems(obsList);
		categoryListView.getSelectionModel().select(0);
		
	}
	
	/**
	 * Creates new tag based on entered text
	 * Checks for duplicate tags
	 * @param event
	 */
	public void createTag(ActionEvent event) {
		Alert duplicateTagAlert = new Alert(AlertType.ERROR);
		duplicateTagAlert.setTitle("Duplicate Tag");
		duplicateTagAlert.setHeaderText("Tag already exists");
		duplicateTagAlert.setContentText("A Tag may not be added to a photo more than once");
		
		Alert duplicateLocationTag = new Alert(AlertType.ERROR);
		duplicateLocationTag.setTitle("Duplicate Location Tag");
		duplicateLocationTag.setHeaderText("Location Tag already exists");
		duplicateLocationTag.setContentText("A photo may not have more than one location tag");
		if(currentPhoto.searchTags(categoryListView.getSelectionModel().getSelectedItem(), valueField.getText())) {
			duplicateTagAlert.showAndWait();
		}
		else if(currentPhoto.containsLoctionTag() && categoryListView.getSelectionModel().getSelectedItem().equals("location")){
				duplicateLocationTag.showAndWait();
		}
		else {
		currentPhoto.addTag(categoryListView.getSelectionModel().getSelectedItem(), valueField.getText());
		closeStage(event);
		}
	}
	
	/**
	 * Opens custom tag view for creating custom categories
	 * @param event
	 * @throws IOException
	 */
	public void customCategory(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/photos/view/addTagView.fxml"));
		Parent rootParent = loader.load();
		
		AddTagViewController addTagViewController = loader.getController();
		addTagViewController.setupTagDialog(currentPhoto, currentUser);
		
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
    	scene = new Scene(rootParent);
    	stage.setScene(scene);
    	stage.show();
	}
	
	/**
	 * closes window
	 * @param event
	 */
	public void cancel(ActionEvent event) {
		closeStage(event);
	}
	
	/**
	 * closes window after completing action
	 * @param event
	 */
	private void closeStage(ActionEvent event) {
		Node source = (Node)event.getSource();
		Stage stage = (Stage)source.getScene().getWindow();
		stage.close();
	}

}
