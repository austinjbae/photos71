package photos.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import photos.app.Album;
import photos.app.Photo;
import photos.app.Tag;
import photos.app.User;
import photos.app.UserList;

/**
 * This class controls the photoView.fxml view
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class PhotoViewController {
	
	private Stage stage;
	private Scene scene;
	private Parent rootParent;
	private User user;
	private UserList userList;
	private Album currentAlbum;
	
	@FXML
	private Label photoLibrary;
	@FXML
	private Button btnAdd;
	@FXML
	private Button btnBack;
	@FXML
	private Button btnDelete;
	@FXML
	private Button btnCaption;
	@FXML
	private Button btnAddTag;
	@FXML
	private Button btnDeleteTag;
	@FXML
	private Button btnCopyPhoto;
	@FXML
	private Button btnMovePhoto;
	@FXML
	private Button btnSlideshow;
	@FXML
	private TableView<Photo> photoTable;
	@FXML
	private TableColumn<Photo, String> dateCol;
	@FXML
	private TableColumn<Photo, String> captionCol;
	@FXML
	private ImageView imageView;
	@FXML
	private ListView<String> tagsListView;
	
	private ObservableList<Photo> photosArrayList = FXCollections.observableArrayList();
	private ObservableList<String> tagArrayList = FXCollections.observableArrayList();
	ArrayList<Photo> userPhotos;
	FileChooser fileChooser = new FileChooser();
	File selectedFile;
	Photo currentPhoto;
	TextInputDialog captionDialog = new TextInputDialog();
	String newCaption;
	Image image;
	Alert deleteConfirm = new Alert(AlertType.CONFIRMATION);

	/**
	 * Initializes photo table with photos within inputed album
	 * Contains actions for adding, deleting, and captioning photos
	 * Contains actions for adding and deleting tags
	 * Contains actions for moving and copying photos
	 * Contains action for opening slide show
	 * @param userList
	 * @param user
	 * @param album
	 */
	public void loadAlbum(UserList userList, User user, Album album) {
		this.userList = userList;
		this.user = user;
		currentAlbum = album;
		photoLibrary.setText("Album: " + currentAlbum.albumName);
		
		userPhotos = currentAlbum.photos;
		for(int i = 0; i < currentAlbum.photos.size(); i++) {
			photosArrayList.add(currentAlbum.photos.get(i));
		}
		
		photoTable.setItems(photosArrayList);
		photoTable.getColumns().setAll(dateCol, captionCol);
		
		dateCol.setCellValueFactory(new PropertyValueFactory<Photo, String>("photoDate"));
		captionCol.setCellValueFactory(new PropertyValueFactory<Photo, String>("caption"));
		
		photoTable.getSelectionModel().select(0);
		
		Alert nullSelectionAlert = new Alert(AlertType.ERROR);
		nullSelectionAlert.setTitle("Selection Error");
		nullSelectionAlert.setHeaderText("Select an item");
		nullSelectionAlert.setContentText("An item must be selected in the list before taking action");
		
		Alert duplicatePhotoAlert = new Alert(AlertType.ERROR);
		duplicatePhotoAlert.setTitle("Duplicate Photo");
		duplicatePhotoAlert.setHeaderText("Photo already exists");
		duplicatePhotoAlert.setContentText("A photo may not be added to an album more than once");
		
		tagsDisplay();	
		
		showInfo(stage);
		photoTable.getSelectionModel().selectedIndexProperty().addListener(e -> {
			showInfo(stage);
			tagsDisplay();
		});
		
		
		fileChooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter("JPEG image", "*.jpg"),
				new FileChooser.ExtensionFilter("BMP image", "*.bmp"),
				new FileChooser.ExtensionFilter("GIF image", "*.gif"),
				new FileChooser.ExtensionFilter("PNG image", "*.png"));
		btnAdd.setOnAction(e ->{
			selectedFile = fileChooser.showOpenDialog(stage);
			currentPhoto = new Photo(selectedFile);
			if(currentAlbum.duplicatePhoto(currentPhoto)) {
				duplicatePhotoAlert.showAndWait();
			}
			else {
			photosArrayList.add(currentPhoto);
			currentAlbum.addPhoto(currentPhoto);
			photoTable.getSelectionModel().select(currentPhoto);
			showInfo(stage);
			}
		});
		
		captionDialog.setContentText("Caption: ");
		captionDialog.setHeaderText("Enter a Caption");
		captionDialog.setTitle("Caption Photo");
		btnCaption.setOnAction(e ->{
			if(photoTable.getSelectionModel().getSelectedItem() == null) {
	    		nullSelectionAlert.showAndWait();
	    	}
	    	else {
			captionDialog.showAndWait();
			newCaption = captionDialog.getEditor().getText();
			photoTable.getSelectionModel().getSelectedItem().caption = newCaption;
			photoTable.refresh();
	    	}
		});
		
		deleteConfirm.setTitle("Confirm Deletion");
		deleteConfirm.setHeaderText("Delete Photo?");
		deleteConfirm.setContentText("Are you sure you want to delete the selected photo?");
		btnDelete.setOnAction(e -> {
			if(photoTable.getSelectionModel().getSelectedItem() == null) {
	    		nullSelectionAlert.showAndWait();
	    	}
	    	else {
	    	Optional<ButtonType> result = deleteConfirm.showAndWait();
	    	if(result.get() == ButtonType.OK) {
			deleteConfirm.showAndWait();
			userPhotos.remove(photosArrayList.indexOf(photoTable.getSelectionModel().getSelectedItem()));
			photosArrayList.remove(photosArrayList.indexOf(photoTable.getSelectionModel().getSelectedItem()));
			photoTable.refresh();
	    	}
	    	}
		});
		
		btnAddTag.setOnAction(e -> {
			if(photoTable.getSelectionModel().getSelectedItem() == null) {
	    		nullSelectionAlert.showAndWait();
	    	}
	    	else {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/photos/view/tagEditorView.fxml"));
			Parent parent = null;
			try {
				parent = fxmlLoader.load();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			TagEditorViewController tagEditorViewController = fxmlLoader.<TagEditorViewController>getController();
			tagEditorViewController.initialize(photoTable.getSelectionModel().getSelectedItem(), user);
			
			Scene scene = new Scene(parent);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.showAndWait();
			tagsDisplay();
	    	}
		});
		
		btnDeleteTag.setOnAction(e -> {
			if(photoTable.getSelectionModel().getSelectedItem() == null) {
	    		nullSelectionAlert.showAndWait();
	    	}
	    	else {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/photos/view/deleteTagView.fxml"));
			Parent parent = null;
			try {
				parent = fxmlLoader.load();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			DeleteTagViewController deleteTagViewController = fxmlLoader.<DeleteTagViewController>getController();
			deleteTagViewController.setupList(photoTable.getSelectionModel().getSelectedItem());
			
			Scene scene = new Scene(parent);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.showAndWait();
			tagsDisplay();
	    	}
		});
		
		btnMovePhoto.setOnAction(e -> {
			if(photoTable.getSelectionModel().getSelectedItem() == null) {
	    		nullSelectionAlert.showAndWait();
	    	}
	    	else {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/photos/view/copyMoveView.fxml"));
			Parent parent = null;
			try {
				parent = fxmlLoader.load();
			} catch(IOException e2) {
				e2.printStackTrace();
			}
			CopyMoveViewController copyMoveViewController = fxmlLoader.<CopyMoveViewController>getController();
			copyMoveViewController.setupList(user, photoTable.getSelectionModel().getSelectedItem(), currentAlbum, true);
			
			Scene scene = new Scene(parent);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.showAndWait();
			photosArrayList.remove(photoTable.getSelectionModel().getSelectedItem());
			photoTable.refresh();
	    	}
		});
		
		btnCopyPhoto.setOnAction(e -> {
			if(photoTable.getSelectionModel().getSelectedItem() == null) {
	    		nullSelectionAlert.showAndWait();
	    	}
	    	else {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/photos/view/copyMoveView.fxml"));
			Parent parent = null;
			try {
				parent = fxmlLoader.load();
			} catch(IOException e2) {
				e2.printStackTrace();
			}
			CopyMoveViewController copyMoveViewController = fxmlLoader.<CopyMoveViewController>getController();
			copyMoveViewController.setupList(user, photoTable.getSelectionModel().getSelectedItem(), currentAlbum, false);
			
			Scene scene = new Scene(parent);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.showAndWait();
	    	}
		});
		
		
		btnBack.setOnAction(e -> {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/photos/view/albumView.fxml"));
			Parent rootParent = null;
			try {
				rootParent = loader.load();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			AlbumViewController albumViewController = loader.getController();
			albumViewController.loadUser(userList, user);
			
			Node source = (Node)e.getSource();
			stage = (Stage)source.getScene().getWindow();
			scene = new Scene(rootParent);
			stage.setScene(scene);
			stage.show();
		});
		
		btnSlideshow.setOnAction(e -> {
			if(photoTable.getSelectionModel().getSelectedItem() == null) {
	    		nullSelectionAlert.showAndWait();
	    	}
	    	else {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/photos/view/slideshowView.fxml"));
			Parent rootParent = null;
			try {
				rootParent = loader.load();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			SlideshowViewController slideshowViewController = loader.getController();
			slideshowViewController.slideshow(photoTable.getSelectionModel().getSelectedItem(), currentAlbum, userList, user);
			
			Node source = (Node)e.getSource();
			stage = (Stage)source.getScene().getWindow();
			scene = new Scene(rootParent);
			stage.setScene(scene);
			stage.show();
	    	}
		});
	}
	
	/**
	 * Initializes list of tags for selected photo
	 */
	private void tagsDisplay() {
		if(photoTable.getSelectionModel().getSelectedItem() != null) {
		tagArrayList = FXCollections.observableArrayList();
		for(int i = 0; i < photoTable.getSelectionModel().getSelectedItem().tags.size(); i++) {
			tagArrayList.add(photoTable.getSelectionModel().getSelectedItem().tags.get(i).category + ": " + photoTable.getSelectionModel().getSelectedItem().tags.get(i).name);
		}
		
		tagsListView.setItems(tagArrayList);
		}
	}
	
	/**
	 * Displays selected photo
	 * @param mainStage
	 */
	private void showInfo(Stage mainStage) {
		if(photosArrayList.size() > 0) {
			InputStream stream;
			try {
				stream = new FileInputStream(photoTable.getSelectionModel().getSelectedItem().photo);
				image = new Image(stream);
				imageView.setImage(image);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			}
			}
		else {
			imageView.setImage(null);
		}
	}

}
