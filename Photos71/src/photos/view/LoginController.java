package photos.view;

import java.io.IOException;

import photos.app.Album;
import photos.app.DataStorage;
import photos.app.Photo;
import photos.app.User;
import photos.app.UserList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * This class controls the photosLogin.fxml view
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class LoginController{

	private Stage stage;
	private Scene scene;
	private Parent rootParent;
	private UserList userList;
	private User currentUser;
	
    @FXML
    private Button addImage;
    @FXML
    private AnchorPane root;  
    @FXML
    private Button btnLogin;
    @FXML
    private TextField loginText;
    @FXML
    private ListView<User> listviewUsers;
    @FXML
    private TextField adminTextfield;
    @FXML
    private Button addUser;
    @FXML
    private Button deleteUser;
    @FXML
    private Button adminExit;
    
    /**
     * Will log the user in if they input a valid username
     * @param event
     * @throws IOException
     */
    @FXML
    void logIn(ActionEvent event) throws IOException {
    	String name = loginText.getText();
    	
    	try {
    		if(name.equals("admin")) {
    			switchToAdmin(event);
    		} else {
    			if(this.userList.getUserListString().contains(name)) {
    				for(int i = 0; i < userList.getUserList().size(); i++) {
    					if(userList.getUserList().get(i).getName().equals(name)) {
    						this.currentUser = userList.getUserList().get(i);
    						switchToWorkspace(event);
    					}
    				}
    			} else {
    				Alert alert = new Alert(AlertType.ERROR, "No User with matching username");
    				alert.showAndWait();
    			}
    		}
    	} catch (IllegalArgumentException i) {
    		
    	}
    	
    }
    
    /**
     * Switches the scene to the Photos App Workspace
     * @param event
     * @throws IOException
     */
    public void switchToWorkspace(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("/photos/view/albumView.fxml"));
    	Parent rootParent = loader.load();
   
    	AlbumViewController albumViewController = loader.getController();
    	albumViewController.loadUser(this.userList, this.currentUser);
    	
    	stage = (Stage)((Node)event.getSource()).getScene().getWindow();
    	scene = new Scene(rootParent);
    	stage.setScene(scene);
    	stage.show();
    }
      
    /**
     * Switches the scene to the Photos App Admin Interface
     * @param event
     * @throws IOException
     */
    public void switchToAdmin(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("/photos/view/photosAdmin.fxml"));
    	Parent rootParent = loader.load();
    	//rootParent = FXMLLoader.load(getClass().getResource("/photos/view/photosAdmin.fxml"));
   
    	AdminController adminController = loader.getController();
    	adminController.populateListView(this.userList);
    	
    	stage = (Stage)((Node)event.getSource()).getScene().getWindow();
    	scene = new Scene(rootParent);
    	stage.setScene(scene);
    	stage.show();
    }

    /**
     * Loads the UserList for the LoginController after being manipulated by other controllers
     * @param userList
     */
    public void loadUserList(UserList userList) {
    	this.userList = userList;
    }
    
    /**
     * Creates the stock user and very first UserList upon application startup
     * Deserializing the UserList should occur here
     * @throws IOException 
     * @throws ClassNotFoundException 
     */
	public void start() throws ClassNotFoundException, IOException {
		// TODO Auto-generated method stub
		
	    String stockname = "stock";
	    User stock = new User(stockname);
	    UserList userList = DataStorage.readData();
//	    userList.addUser(stock);
//	    stock.albums.add(new Album("stock photos"));
//	    stock.albums.get(0).photos.add(new Photo("./stock/beach.jpg"));
//	    stock.albums.get(0).photos.add(new Photo("./stock/camel.jpg"));
//	    stock.albums.get(0).photos.add(new Photo("./stock/city.jpg"));
//	    stock.albums.get(0).photos.add(new Photo("./stock/clouds.jpg"));
//	    stock.albums.get(0).photos.add(new Photo("./stock/flower.jpg"));
//	    stock.albums.get(0).photos.add(new Photo("./stock/wallpaper.jpg"));
	    
	    //UserList userList = new UserList();
	    this.userList = userList;
		
	}


}
