package photos.view;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import photos.app.Album;
import photos.app.Photo;
import photos.app.User;
import photos.app.UserList;

/**
 * This class controls the slideshowView.fxml view
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class SlideshowViewController {
	
	@FXML
	private ImageView imageView;
	@FXML
	private Button btnNext;
	@FXML
	private Button btnPrevious;
	@FXML
	private Button btnBack;
	
	Photo currentPhoto;
	Album currentAlbum;
	UserList users;
	User user;
	
	/**
	 * initializes class fields
	 * contains actions for traversing list of photos
	 * @param currentPhoto
	 * @param currentAlbum
	 * @param users
	 * @param user
	 */
	public void slideshow(Photo currentPhoto, Album currentAlbum, UserList users, User user) {
		this.currentPhoto = currentPhoto;
		this.currentAlbum = currentAlbum;
		this.users = users;
		this.user = user;
		
		showImage();
		
		btnNext.setOnAction(e -> {
			advanceImage();
		});
		
		btnPrevious.setOnAction(e -> {
			previousImage();
		});
		
		btnBack.setOnAction(e -> {
			backToAlbum(e);
		});
		
		
	}
	
	/**
	 * Displays the next image in the album
	 * Returns to beginning if end of album is reached
	 */
	public void advanceImage() {
		if(currentAlbum.photos.indexOf(currentPhoto) + 1 == currentAlbum.photos.size()) {
			currentPhoto = currentAlbum.photos.get(0);
		}
		else {
			currentPhoto = currentAlbum.photos.get(currentAlbum.photos.indexOf(currentPhoto) + 1);
		}
		showImage();
	}
	
	/**
	 * Displays the previous image in the album
	 * Returns to end if beginning of album is reached
	 */
	public void previousImage() {
		if(currentAlbum.photos.indexOf(currentPhoto) - 1 < 0) {
			currentPhoto = currentAlbum.photos.get(currentAlbum.photos.size() - 1);
		}
		else {
			currentPhoto = currentAlbum.photos.get(currentAlbum.photos.indexOf(currentPhoto) - 1);
		}
		showImage();
	}
	
	/**
	 * Displays the current image
	 */
	public void showImage() {
		InputStream stream;
		try {
			stream = new FileInputStream(currentPhoto.photo);
			Image image = new Image(stream);
			imageView.setImage(image);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * returns to photo view
	 * @param e
	 */
	public void backToAlbum(ActionEvent e) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/photos/view/photoView.fxml"));
		Parent rootParent = null;
		try {
			rootParent = loader.load();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PhotoViewController photoViewController = loader.getController();
		photoViewController.loadAlbum(users, user, currentAlbum);
		
		Node source = (Node)e.getSource();
		Stage stage = (Stage)source.getScene().getWindow();
		Scene scene = new Scene(rootParent);
		stage.setScene(scene);
		stage.show();
	}

}
