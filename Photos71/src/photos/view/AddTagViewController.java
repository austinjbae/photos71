package photos.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import photos.app.Photo;
import photos.app.User;

/**
 * this class controls the addTagView.fxml view
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class AddTagViewController {
	
	@FXML
	private TextField categoryField;
	@FXML
	private TextField nameField;
	@FXML
	private Button btnCreate;
	@FXML
	private Button btnCancel;
	@FXML
	private Button btnCreatePreset;
	
	Photo currentPhoto;
	User currentUser;
	
	/**
	 * Initializes class fields
	 * @param currentPhoto
	 * @param currentUser
	 */
	public void setupTagDialog(Photo currentPhoto, User currentUser) {
		this.currentPhoto = currentPhoto;
		this.currentUser = currentUser;
	}
	
	/**
	 * Creates new tag when create button is pressed
	 * @param event
	 */
	public void createTag(ActionEvent event) {
		Alert duplicateTagAlert = new Alert(AlertType.ERROR);
		duplicateTagAlert.setTitle("Duplicate Tag");
		duplicateTagAlert.setHeaderText("Tag already exists");
		duplicateTagAlert.setContentText("A Tag may not be added to a photo more than once");
		
		Alert duplicateLocationTag = new Alert(AlertType.ERROR);
		duplicateLocationTag.setTitle("Duplicate Location Tag");
		duplicateLocationTag.setHeaderText("Location Tag already exists");
		duplicateLocationTag.setContentText("A photo may not have more than one location tag");
		if(currentPhoto.searchTags(categoryField.getText(), nameField.getText())) {
			duplicateTagAlert.showAndWait();
		}
		else if(currentPhoto.containsLoctionTag() && categoryField.getText().equals("location")){
			duplicateLocationTag.showAndWait();
		}
		else {
		currentPhoto.addTag(categoryField.getText(), nameField.getText());
		closeStage(event);
		}
	}
	
	/**
	 * Creates new tag and adds tag category to default list when button is pressed
	 * @param event
	 */
	public void createPreset(ActionEvent event) {
		Alert duplicateTagAlert = new Alert(AlertType.ERROR);
		duplicateTagAlert.setTitle("Duplicate Tag");
		duplicateTagAlert.setHeaderText("Tag already exists");
		duplicateTagAlert.setContentText("A Tag may not be added to a photo more than once");
		
		Alert duplicateLocationTag = new Alert(AlertType.ERROR);
		duplicateLocationTag.setTitle("Duplicate Location Tag");
		duplicateLocationTag.setHeaderText("Location Tag already exists");
		duplicateLocationTag.setContentText("A photo may not have more than one location tag");
		if(currentPhoto.searchTags(categoryField.getText(), nameField.getText())) {
			duplicateTagAlert.showAndWait();
		}
		else if(currentPhoto.containsLoctionTag() && categoryField.getText().equals("location")){
			duplicateLocationTag.showAndWait();
		}
		else {
		currentPhoto.addTag(categoryField.getText(), nameField.getText());
		currentUser.presets.add(categoryField.getText());
		closeStage(event);
		}
	}
	
	/**
	 * Closes window when cancel button is pressed
	 * @param event
	 */
	public void cancelTag(ActionEvent event) {
		closeStage(event);
	}
	
	/**
	 * Closes window upon action
	 * @param event
	 */
	private void closeStage(ActionEvent event) {
		Node source = (Node)event.getSource();
		Stage stage = (Stage)source.getScene().getWindow();
		stage.close();
	}

}
