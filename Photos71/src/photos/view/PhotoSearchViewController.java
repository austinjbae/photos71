package photos.view;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import photos.app.Photo;
import photos.app.PhotoSearch;
import photos.app.Tag;
import photos.app.User;
import photos.app.UserList;

/**
 * This class controls the photoSearchView.fxml view
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class PhotoSearchViewController {
	
	@FXML
	private TextField beginDateField;
	@FXML
	private TextField endDateField;
	@FXML
	private TextField singleTagCategoryField;
	@FXML
	private TextField singleTagValueField;
	@FXML
	private TextField doubleTagCategoryField1;
	@FXML
	private TextField doubleTagValueField1;
	@FXML
	private TextField doubleTagCategoryField2;
	@FXML
	private TextField doubleTagValueField2;
	@FXML
	private Button btnDateSearch;
	@FXML
	private Button btnSingleSearch;
	@FXML
	private Button btnConjunctiveSearch;
	@FXML
	private Button btnDisjunctiveSearch;
	@FXML
	private Button btnCancel;
	
	UserList userList;
	User currentUser;
	ArrayList<Photo> searchResults;
	Stage stage;
	Scene scene;
	
	/**
	 * Initialize class fields
	 * @param userList
	 * @param currentUser
	 */
	public void initialize(UserList userList, User currentUser) {
		this.userList = userList;
		this.currentUser = currentUser;
	}
	
	/**
	 * Executes a search based on date parameters
	 * @param event
	 * @throws ParseException
	 * @throws IOException
	 */
	public void dateSearch(ActionEvent event) throws ParseException, IOException {
		searchResults = PhotoSearch.photoSearch(currentUser, beginDateField.getText(), endDateField.getText());
		displayResults(searchResults, event);
	}
	
	/**
	 * Executes a search based on tag parameter
	 * @param event
	 * @throws IOException
	 */
	public void singleSearch(ActionEvent event) throws IOException {
		searchResults = PhotoSearch.photoSearch(currentUser, new Tag(singleTagCategoryField.getText(), singleTagValueField.getText()));
		displayResults(searchResults, event);
	}
	
	/**
	 * Executes a search based on two tag parameters
	 * @param event
	 * @throws IOException
	 */
	public void conjunctiveSearch(ActionEvent event) throws IOException {
		Tag tag1 = new Tag(doubleTagCategoryField1.getText(), doubleTagValueField1.getText());
		Tag tag2 = new Tag(doubleTagCategoryField2.getText(), doubleTagValueField2.getText());
		searchResults = PhotoSearch.photoSearchConjunctive(currentUser, tag1, tag2);
		displayResults(searchResults, event);
	}
	
	/**
	 * Executes a search based on two tag parameters
	 * @param event
	 * @throws IOException
	 */
	public void disjunctiveSearch(ActionEvent event) throws IOException {
		Tag tag1 = new Tag(doubleTagCategoryField1.getText(), doubleTagValueField1.getText());
		Tag tag2 = new Tag(doubleTagCategoryField2.getText(), doubleTagValueField2.getText());
		searchResults = PhotoSearch.photoSearchDisjunctive(currentUser, tag1, tag2);
		displayResults(searchResults, event);
	
	}
	
	/**
	 * Changes window to display search results
	 * @param results
	 * @param event
	 * @throws IOException
	 */
	public void displayResults(ArrayList<Photo> results, ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("/photos/view/searchResultsView.fxml"));
    	Parent rootParent = loader.load();

    	SearchResultViewController searchResultViewController = loader.getController();
    	searchResultViewController.initialize(userList, currentUser, results);
    	
    	stage = (Stage)((Node)event.getSource()).getScene().getWindow();
    	scene = new Scene(rootParent);
    	stage.setScene(scene);
    	stage.show();
	}
	
	/**
	 * returns to Album view window
	 * @param event
	 */
	public void cancel(ActionEvent event) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/photos/view/albumView.fxml"));
		Parent rootParent = null;
		try {
			rootParent = loader.load();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		AlbumViewController albumViewController = loader.getController();
		albumViewController.loadUser(userList, currentUser);
		
		Node source = (Node)event.getSource();
		stage = (Stage)source.getScene().getWindow();
		scene = new Scene(rootParent);
		stage.setScene(scene);
		stage.show();
	}

}
